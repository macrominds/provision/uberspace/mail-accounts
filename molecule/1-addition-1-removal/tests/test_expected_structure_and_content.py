import pytest


@pytest.mark.parametrize("user", [
    "john",
    "bob",
])
def test_mail_accounts_present(host, user):
    assert_mail_account_present(host.file(get_mail_account_folder(user)))


def assert_mail_account_present(file):
    assert file.exists
    assert 'ansible' == file.user
    assert 'ansible' == file.group


def get_mail_account_folder(user):
    return '/home/ansible/users/{}'.format(user)


@pytest.mark.parametrize("user", [
    "jane",
    "alice",
])
def test_mail_accounts_gone(host, user):
    assert_mail_account_gone(host.file(get_mail_account_folder(user)))


def assert_mail_account_gone(file):
    assert not file.exists
