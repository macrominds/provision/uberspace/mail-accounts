# macrominds/provision/uberspace/mail-accounts

Setup mail accounts on an uberspace.

Missing mail accounts will be created. A password will automatically be provided. 
You can look it up in the `.credentials` subdirectory, relative to the playbook that has been run.
The `.credentials` subdirectory is not version controlled.

**Important**: Mail accounts that are present on the server, but not specified in the `mail_accounts` list,
**will be removed** without further confirmation. Make sure to list all accounts, you want to keep.

Changing the password is not part of this playbook. This needs to be performed manually if desired.
Do so by invoking `uberspace mail user password`.

## Requirements

Setup an [U7 Uberspace](https://dashboard.uberspace.de/register) and provide an ssh access to it.

## Role Variables

See [defaults/main.yml](defaults/main.yml). 

* `mail_accounts`: 
  The list of desired mail accounts. E.g. ['info', 'contact']. See [Playbook example](#playbook).
* `mail_accounts_tool_chdir`: the folder from which to execute the following commands.
  Defaults to the user's home directory: `"/home/{{ ansible_facts.user_id }}/users"`
* `mail_accounts_tool_add_command`:  the command to add an account.
  Defaults to: `"uberspace mail user add"`
* `mail_accounts_tool_password_param`: the password parameter prefix. 
  Defaults to: `"-p "`
* `mail_accounts_tool_del_command`: the command to remove an account.
  Defaults to: `"uberspace mail user del"`
* `mail_accounts_tool_command_warnings`: if warnings should be issued for typical command misuse like touch, ls, etc.
  This is used to turn off warnings during testing, because the mocked commands would trigger a warning. 
  Defaults to: `true`

## Example Playbook

### Prerequisites

In your project, provide the following files:

ansible.cfg

```ini
[defaults]
roles_path = $PWD/galaxy_roles:$PWD/roles
```

requirements.yml
 
```yaml
- src: git+https://gitlab.com/macrominds/provision/uberspace/mail-accounts.git
  path: roles
  name: mail-accounts
```

And run `ansible-galaxy install -r requirements.yml` to install 
or `ansible-galaxy install -r requirements.yml  --force` to upgrade.

### Playbook

```yaml
- hosts: all
  roles:
    - role: mail-accounts
      mail_accounts:
        - info
        - contact
```

## Testing

Test this role with `molecule test --all`.

## License

ISC

## Author Information

This role was created in 2020 by Thomas Praxl.
